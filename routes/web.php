<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\JoinController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RelationController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();

Route::group(
    [
        'prefix' => 'store',
    ], function () {

        Route::resource('product', ProductController::class);

         Route::resource('category', CategoryController::class); 
        

        Route::resource('roles', RoleController::class);

        Route::resource('uesrs', UserController::class);

    });

Route::get('/', function () {
    return view('welcome');
});

Route::get('/join', [JoinController::class, 'show']);

Route::get('/relation', [RelationController::class, 'index'])->name('relation');

Route::get('about', [AboutController::class, 'about'])->name('about');

Route::get('contact', [AboutController::class, 'contact'])->name('contact');

Route::get('import-form', [ProductController::class, 'importForm'])->name('importForm');

Route::post('import',[ProductController::class,'import'])->name('import');

Route::get('export', [ProductController::class, 'export'])->name('export'); 

Route::get('categoryexport', [CategoryController::class, 'export'])->name('categoryexport'); 