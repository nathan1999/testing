<?php

namespace App\Providers;

use App\Repositories\category\categoryInterface;
use App\Repositories\category\CategoryRepositories;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\category\categoryInterface', 'App\Repositories\category\CategoryRepositories');

        $this->app->bind('App\Repositories\product\ProductInterface', 'App\Repositories\product\ProductRepositories');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
