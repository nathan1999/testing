<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = "products";

    protected $fillable = [
        'Product_name', 'Product_description', 'Product_image', 'Product_code'
    ];
    

    public function category()
    {
      return $this->belongsToMany(Category::class,'category_product');
    }

}
