<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    use HasFactory;

        public function Member()
        {
            return $this->belongsTo('App\Models\Member');
        }
}
