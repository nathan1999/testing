<?php

namespace App\Http\Controllers;

use App\Exports\CategoryExport;
use App\Models\Category;
use App\Repositories\category\CategoryRepositories;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;


class CategoryController extends Controller
{
    private $CategoryRepositories;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(CategoryRepositories $CategoryRepositories)
    {
        $this->CategoryRepositories = $CategoryRepositories;

        $this->middleware(['role_or_permission:admin']);
    }

    public function index(Request $request)
    {
        $data=$this->CategoryRepositories->all();

        return view('category.index',$data);
    }

    /**
     * Show the form for creating a new resource.
        *
        * @return \Illuminate\Http\Response
        */
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
       $this->CategoryRepositories->store($request);
        return json_encode(array(
            "statusCode"=>200
        ));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $data=$this->CategoryRepositories->show();
        
        return view('category.show',$data,['catogries' => Category::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('category.edit_category', ['categories'=>Category::FindOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categories=$this->CategoryRepositories->update($request,$id);
        return json_encode(array('statusCode'=>200));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {   
        $this->CategoryRepositories->delete($id);
        return redirect()->route('category.index');
    }   

    public function export()
    {
        return Excel::download(new CategoryExport, 'category.xlsx');
    }
}
