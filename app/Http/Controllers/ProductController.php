<?php

namespace App\Http\Controllers;

use App\Exports\ProductExport;
use App\Imports\ProductImport;
use App\Models\Category;
use App\Models\Product;
use App\Repositories\product\ProductRepositories;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    public $ProductRepositories;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(ProductRepositories $ProductRepositories)
    {
        $this->ProductRepositories = $ProductRepositories;
        $this->Middleware('auth');
    }

    public function index(Request $request)
    {
        $data = $this->ProductRepositories->all();

        return view('product.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('home', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $product = Product::create([
            'Product_name' => $request->Product_name,
            'Product_description' => $request->Product_description,
            'Product_image' => $request->Product_image,
            'Product_code' => $request->Product_code,
        ]);

        $product->category()->attach($request->Category);

        return redirect()->route('product.index')->with('success', 'product added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::all();
        return view('product.show', compact('category'), ['product' => Product::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $category = Category::all();
        return view('product.edit', compact('product', 'category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $product = Product::find($id);

        $product->Product_name = $request->Product_name;
        $product->Product_description = $request->Product_description;
        $product->Product_code = $request->Product_code;
        $product->category()->sync($request->category);
        $product->save();
        return redirect()->route('product.index')->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        echo ("User Record deleted successfully.");
        return redirect()->route('product.index');
    }

    public function export()
    {
        return Excel::download(new ProductExport, 'product.xlsx');
    }

    public function importForm()
    {
        return view('import-form');
    }
    public function import(Request $request)
    {
        Excel::import(new ProductImport, request()->file('file'));
        return back()->with('status', 'Import successfully done');
    }

}
