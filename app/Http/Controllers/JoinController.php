<?php

namespace App\Http\Controllers;

use App\Models\Join;

class JoinController extends Controller
{
    function show()
    {
        $data=Join::join('Categories', 'categories.id', '=', 'products.Category_id')
        ->select('products.Category_id','categories.name')
        ->get();

        return view('category.join_table',compact('data'));


    }
}
