<?php

namespace App\Imports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToModel;


class ProductImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Product([
            'Product_name' => $row[0],
            'Product_description' => $row[1],
            'Product_image' => $row[2],
            'Product_code' => $row[3],
        ]);
    }
}
