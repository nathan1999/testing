<?php

namespace App\Repositories\category;



interface categoryInterface{

    public function all();

    public function store($request);

    public function show();

    public function update($id);

    public function delete($id);
}
