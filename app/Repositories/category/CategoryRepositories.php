<?php

namespace App\Repositories\category;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryRepositories{
    
    public function all(){
      return array(
            'categories' =>Category::all()
           );
          
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        Category::create($request->all());
    }

    public function show()
    {
        return array(
            'products' =>Category::all()
           );
    }

    public function update(Request $request, $id)
    {
        $categories=Category::find($id);

        $categories->name=$request->input('name');
        $categories->save();
    }

    public function delete($id)
    {
        return Category::find($id)->delete();
    }
}