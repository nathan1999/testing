@extends('layouts.master')

@section('title')
    <table class="table">
        <thead>
            <tr>
                <th scope="col">categories.id</th>
                <th scope="col">NAME</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $row)
            <tr>
                <td scope="col">{{ $row->Category_id }}</td>
                <td scope="col">{{ $row->name }}</td>
            </tr>
            @endforeach
                
        </tbody>
    @endsection
</table>
