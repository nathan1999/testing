@extends('layouts.master')

@section('title')
    <table class="table" id="relation">
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">NAME</th>
                <th scope="col">member_id</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $row)
            <tr>
                <td scope="col">{{ $row->id }}</td>
                <td scope="col">{{ $row->name }}</td>
                <td scope="col">{{ $row->member_id }}</td>
            </tr>
            @endforeach
                
        </tbody>
    </table>
<script>
    $(document).ready( function () {
        $('#relation').DataTable();
    } );
</script>
    @endsection

