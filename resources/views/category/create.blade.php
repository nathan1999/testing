@extends('layouts.master')

@section('title')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<form action="" id="createfrm">
    <div class="form-group">
        <input type="hidden" name="_token" id="csrf" value="{{ Session::token() }}">
        <label for="email">Name:</label>
        <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name">
    </div>
    <button type="submit" class="btn btn-primary" id="butsave">Submit</button>
    </div>
</form>
   
    <script>
        $(document).ready(function() {

            $('#butsave').on('click', function() {
                var name = $('#name').val();
                if (name != "") {
                    /*  $("#butsave").attr("disabled", "disabled"); */
                    $.ajax({
                        url: "{{route('category.store')}}",
                        type: "POST",
                        data: {
                            _token: $("#csrf").val(),
                            type: 1,
                            name: name,
                        },
                        cache: false,
                        success: function(dataResult) {
                            console.log(dataResult);
                            var dataResult = JSON.parse(dataResult);
                            if (dataResult.statusCode == 200) {
                                window.location = "{{route('category.index')}}";
                            } else if (dataResult.statusCode == 201) {
                                alert("Error occured !");
                            }

                        }
                    });
                } else {
                    // alert('Please fill all the field !');
                }
            });
        });
    </script>


@endsection
