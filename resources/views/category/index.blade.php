@extends('layouts.master')

@section('content')
       
<div class="container">
    <a class="btn btn-success" href="{{ route('category.create') }}" id="createNewProduct"> Create New category</a>
    
    <table class="table" id="product_table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">NAME</th>
                <th scope="col">Action</th>
            </tr>
        </thead>


        <tbody>
            @foreach ($categories as $categorie)
                <tr>
                    <td scope="col">{{ $categorie->id }}</td>
                    <td scope="col">{{ $categorie->name }}</td>
                    <td scope="col">
                        <form action="{{ route('category.destroy', $categorie->id) }}" method="POST"
                            onsubmit="return confirm('Are you sure want to delete it')">
                            @hasanyrole('delete|admin')
                            <a class="btn btn-info rounded-pill" href="{{ route('category.edit', $categorie->id) }}">EDIT</a>
                            @endhasanyrole

                            <a class="btn btn-warning rounded-pill"
                                href="{{ route('category.show', $categorie->id) }}">SHOW</a>

                            @csrf
                            @method('DELETE')
                            @hasanyrole('delete|admin')
                            <button type="submit" class="btn btn-danger rounded-pill" id="delete">DELETE</button>
                            @endhasanyrole

                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    
<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    $('#product_table').DataTable({

    });
     
    $('#createNewProduct').click(function () {
        $('#saveBtn').val("create-product");
        $('#id').val('');
        $('#productForm').trigger("reset");


    });
    
    $('body').on('click', '.editProduct', function () {
      var id = $(this).data('id');
      $.get("{{ route('category.index') }}" +'/' + id +'/edit', function (data) {
          $('#saveBtn').val("edit-user");
          $('#id').val(data.id);
          $('#name').val(data.name);
      })
   });
    
    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
    
        $.ajax({
          data: $('#productForm').serialize(),
          url: "{{ route('category.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              table.draw();
         
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });
  });
</script>
@endsection
 
