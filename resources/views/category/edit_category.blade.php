@extends('layouts.master')

@section('title')
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" id="name" name="name" value="{{ $categories->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <!-- -->
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button class="btn btn-primary" id="update_data" value="{{ $categories->id }}">Submit</button>
            </div>
        </div>
   
    {{-- </form> --}}
    <script>
        $(document).ready(function(){

    $(document).on("click", "#update_data", function() { 
        var url = "{{route('category.update',$categories->id)}}";
        var id= 
		$.ajax({
			url: url,
			type: "PATCH",
			cache: false,
			data:{
                _token:'{{ csrf_token() }}',
				type: 3,
				name: $('#name').val()
			},
			success: function(dataResult){
                dataResult = JSON.parse(dataResult);
             if(dataResult.statusCode)
             {
                window.location = "{{route('category.index')}}";
             }
             else{
                 alert("Internal Server Error");
             }
				
			}
		});
	}); 
});

    </script>
@endsection
     