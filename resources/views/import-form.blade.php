@extends('layouts.master')

@section('title')
    <div class="container">
        <div class="card bg-light mt-3">
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{session('status')}}
                    </div>
                @endif
                <form action="{{ route('import') }}" id="importfrm" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="file" name="file" class="form-control">
                    <br>
                    <button class="btn btn-success">Import Product Data</button>
                    <a class="btn btn-warning" href="{{ route('export') }}">Export Product Data</a>
                    <a class="btn btn-warning" href="{{ route('categoryexport') }}">Export Category Data</a>
                </form>
            </div>
        </div>
    </div>
@endsection
