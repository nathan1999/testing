@extends('layouts.app')

@section('content') 
    <div class="container">
        <form>
            <div class="form-group">
                <label for="name">Product Name</label>
                <input type="text"  value="{{$product['Product_name']}}" class="form-control">
            </div>
            <div class="form-group">
                <label for="name">Product Description</label>
                <input type="text" value="{{$product['Product_description']}}" class="form-control">
            </div>
            <div class="form-group">
                <label for="name">Product Code</label>
                <input type="text"  value="{{$product['Product_code']}}" class="form-control">
            </div>

            <div class="form-group">
                <label for="name">Product Image</label>
                <input type="text"  value="{{$product['Product_image']}}" class="form-control">
            </div>

            <div class="form-group">
                <label for="name">Category_Name</label>   
                @foreach ($category as $item)
                <input type="text"  value="{{$item['name']}}" class="form-control">
                    @endforeach
            </div>
        </form>
@endsection


