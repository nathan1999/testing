@extends('layouts.master')

@section('content')
    <div>
        @hasanyrole('create|admin')
        <a class="btn btn-primary rounded-pill" href="{{ route('product.create') }}"> Create New product</a>
        @endhasanyrole
    </div><br>

    <table class="table" id="product_table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">PRODUCT_NAME</th>
                <th scope="col">PRODUCT_DESCRIPTION</th>
                <th scope="col">PRODUCT_IMAGE</th>
                <th scope="col">PRODUCT_CODE</th>
                <th scope="col">OPERATION</th>
            </tr>
        </thead>


        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td scope="col">{{ $product->id }}</td>
                    <td scope="col">{{ $product->Product_name }}</td>
                    <td scope="col">{{ $product->Product_description }}</td>
                    <td scope="col">{{ $product->Product_image }}</td>
                    <td scope="col">{{ $product->Product_code }}</td>
                    <td scope="col">
                        <form action="{{ route('product.destroy', $product->id) }}" method="POST"
                            onsubmit="return confirm('Are you sure want to delete it')">
                            @hasanyrole('delete|admin')
                            <a class="btn btn-info rounded-pill" href="{{ route('product.edit', $product->id) }}">EDIT</a>
                            @endhasanyrole

                            <a class="btn btn-warning rounded-pill"
                                href="{{ route('product.show', $product->id) }}">SHOW</a>

                            @csrf
                            @method('DELETE')
                            @hasanyrole('delete|admin')
                            <button type="submit" class="btn btn-danger rounded-pill" id="delete">DELETE</button>
                            @endhasanyrole

                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <script type="text/javascript">
        $('#product_table').DataTable({

        });
    </script>
@endsection
