@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('product.update', $product->id) }}" method="POST" id="regform"
            enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="id" value="{{ $product->id }}">
            <div class="form-group">
                <label for="name">Product Name</label>
                <input type="text" name="Product_name" value="{{ $product->Product_name }}" class="form-control 
                                                                @error('Product_name') is-invalid @enderror ">
                @error('Product_name')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="description">Product Description</label>
                <input type="text" name="Product_description" value="{{ $product->Product_description }}" class="form-control 
                                                                @error('Product_description') is-invalid @enderror ">
                @error('Product_description')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="image">Upload Image</label>
                <input type="file" name="Product_image" value="{{ $product->Product_image }}" class="form-control
                                                                @error('Product_image') is-invalid @enderror ">
                @error('Product_image')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="description">Product code</label>
                <input type="text" name="Product_code" value="{{ $product->Product_code }}" class="form-control
                                                                @error('Product_code') is-invalid @enderror ">
                @error('Product_code')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <select class="form-control category" name="category[]" multiple="multiple">
                    @foreach ($category as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach

                </select>
            </div>
            <div>
                <input type="submit" value="Update" class="btn btn-danger btn-block">
            </div>
        </form>
    </div>
@endsection

@section('script')
    @php
    $category_ids = [];
    @endphp

    @foreach ($product->category as $item)
        @php
            array_push($category_ids, $item->id);
        @endphp
    @endforeach

    <script>
        $(document).ready(function() {
            $('.category').select2();
            data = [];
            data = <?php echo json_encode($category_ids); ?>;
            $('.category').val(data);
            $('.category').trigger('change');
        });
    </script>

@endsection
