@extends('layouts.app')

@section('title')
    <div class="container">
        <form action={{ route('product.store') }}  method="POST" autocomplete="off" id="regform"
            enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">Product Name</label>
                <input type="text" name="Product_name" id="Product_name"
                    value="{{ old('Product_name', optional($product ?? null)->Product_name) }}"
                    class="form-control @error('Product_name') is-invalid @enderror ">
                @error('Product_name')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>



            <div class="form-group">
                <label for="description">Product Description</label>
                <input type="text" name="Product_description"
                    value="{{ old('Product_description', optional($product ?? null)->Product_description) }}"
                    id="Product_description" class="form-control 
                        @error('Product_description') is-invalid @enderror">
                @error('Product_description')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="image">Upload Image</label>
                <input type="file" name="Product_image" id="Product_image" class="form-control
                    @error('Product_image') is-invalid @enderror">
                @error('Product_image')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="description">Product code</label>
                <input type="text" name="Product_code" id="Product_code" value="{{ old('Product_code') }}" class="form-control
                    @error('Product_code') is-invalid @enderror">
                @error('Product_code')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="category">Select Category</label>
                <select name="Category[]" aria-label="Default select example" class="form-control category" multiple="multiple">
                    @error('Category')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                    @foreach ($category as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
    </div>

    <script>
        $(document).ready(function() {
            $('.category').select2();
        });
    </script>

@endsection
