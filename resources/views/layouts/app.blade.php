<!DOCTYPE html>

<html>

<head>
    <title></title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" 
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <style>
        .error {
            color: #FF0000;
        }

    </style>
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-primary bg-light shadow-sm">
            <div class="container">

                <a class="navbar-brand" href="{{ route('product.index') }}">
                    {{ 'Product' }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                @hasanyrole('admin')
                    <a class="navbar-brand" href="{{ route('category.index') }}">
                        {{ 'category' }}
                    </a>
                @endhasanyrole

                <a class="navbar-brand" href="{{ route('about') }}">
                    {{ 'About' }}
                </a>
                <a class="navbar-brand" href="{{ route('contact') }}">
                    {{ 'Contact' }}
                </a>

                <a class="navbar-brand" href="{{ route('relation') }}">
                    {{ 'relation' }}
                </a>

                <a class="navbar-brand" href="{{ route('importForm') }}">
                    {{ 'import' }}
                </a>

                <a class="navbar-brand" href="{{ route('export') }}">
                    {{ 'export' }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right bg-primary" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                                             document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    @hasanyrole('admin')
                                        <a class="dropdown-item" href="{{ route('uesrs.index') }}">
                                            {{ 'manage user' }}
                                        </a>
                                        <a class="dropdown-item" href="{{ route('roles.index') }}">
                                            {{ 'manage Role' }}
                                        </a>
                                    @endhasanyrole


                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav><br>

        @yield('content')

        <div class="container-fluid">
            @yield('title')
        </div>


        {{-- start Footer --}}

    <footer class="text-center text-white fixed-bottom" style="background-color: #21081a;">
        {{-- <div class="container p-4"></div> --}}
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
            © 2020 Copyright:
            <a class="text-white" href="https://mdbootstrap.com/">laravel.com</a>
        </div>
    </footer>
    {{-- end of footer --}}
</body>

<script>
    if ($("#regform").length > 0) {
        $("#regform").validate({

            rules: {
                Product_name: {
                    required: true,
                    maxlength: 50
                },
                Product_description: {
                    required: true,
                    maxlength: 50
                },
                Product_image: {
                    required: true,
                    maxlength: 50
                },
                Product_code: {
                    required: true,
                    maxlength: 50
                },
            },
            messages: {

                Product_name: {
                    required: "Please enter the  name",
                },
                Product_description: {
                    required: "Please enter the  name",
                },
                Product_image: {
                    required: "Please enter the  name",
                },
                Product_code: {
                    required: "Please enter the  name",
                },
            },
        })
    }
</script>

</html> 
