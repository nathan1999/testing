@extends('layouts.master')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Show User</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('uesrs.index') }}"> Back</a>
        </div>
    </div>
</div>

<div class="container">
    <form>
        <div class="form-group">
            <label for="name"> Name</label>
            <input type="text"  value="{{$user->name}}" class="form-control">
        </div>
        <div class="form-group">
            <label for="name"> Email</label>
            <input type="text" value="{{$user->email}}" class="form-control">
        </div>
    </form>
</div>
@endsection