@extends('layouts.master')

@section('content')
    <div class="container">
        <form action="{{ route('uesrs.update', $users->id) }}" method="POST" id="regform"
            enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="id" value="{{ $users->id }}">
        
            <div class="form-group">
                <label for="description">Name</label>
                <input type="text" name="name" value="{{ $users->name }}" class="form-control 
                        @error('name') is-invalid @enderror ">
                @error('name')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="image">Email</label>
                <input type="text" name="email" value="{{ $users->email }}" class="form-control
                        @error('email') is-invalid @enderror ">
                @error('email')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>

            {{--  <div class="form-group">

                <select name="Category" aria-label="Default select example" class="form-control
                        @error('Category') is-invalid @enderror ">
                    @error('Category_id')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                    @foreach ($users as $user)
                        <option value="{{ $user->name }}" @if ($user->name == $product->Category) selected @endif>{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>  --}}

            <div>
                <input type="submit" value="Update" class="btn btn-danger btn-block">
            </div>
        </form>
    </div>
@endsection
