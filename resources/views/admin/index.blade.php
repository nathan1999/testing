@extends('layouts.master')

@section('title')
    <table class="table" id="product_table">
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Role</th>
                <th scope="col">OPERATION</th>
            </tr>
        </thead>
 

    <tbody>
        @foreach ($users as $user)
            <tr>
                <td scope="col">{{ $user->id }}</td>
                <td scope="col">{{ $user->name }}</td>
                <td scope="col">{{ $user->email }}</td>
                <td scope="col"><label class="badge badge-success">{{$user->getRoleNames()}}</label></td>
                <td scope="col">
                    <form action="{{ route('uesrs.destroy', $user->id) }}" method="POST"
                        onsubmit="return confirm('Are you sure want to delete it')">
                        @csrf
                        @method('DELETE')
                        @hasanyrole('delete|admin')
                        <button type="submit" class="btn btn-danger" id="delete">DELETE</button>
                        @endhasanyrole
                       
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>

    
</table>

    <script type="text/javascript">
        $('#product_table').DataTable({
           
        });
    </script>
@endsection
